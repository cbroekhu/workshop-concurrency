package solution.com.workshop.concurrency.util;

public class Util {

   public static void wait( int msecs){

       try {
           Thread.currentThread().sleep(msecs);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }

   }

}

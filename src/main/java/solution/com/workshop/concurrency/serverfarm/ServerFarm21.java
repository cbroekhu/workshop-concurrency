package solution.com.workshop.concurrency.serverfarm;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import solution.com.workshop.concurrency.util.Util;

public class ServerFarm21 {

    public static void main(String[] args) {

        Callable<Server> callable1 = () -> new Server("Apache1" ).startServer();
        Callable<Server> callable2 = () -> new Server("Apache2" ).startServer();
        Callable<Server> callable3 = () -> new Server("Apache3" ).startServer();

        ExecutorService service = Executors.newFixedThreadPool(3);

        List<Future<Server>> futures = new ArrayList<>();
        futures.add( service.submit(callable1));
        futures.add( service.submit(callable2));
        futures.add( service.submit(callable3));

        try {
        	
        	while(true) {
	            for( Future<Server> future: futures) {
	                Util.wait(100);
	            	if(future.isDone()) {
	            		Server server = future.get();
	            		System.out.println("Server " + server.getName() + " is down");
	            	}
	            }
	            if(service.isTerminated()) {
	            	break;
	            }
        	}

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        service.shutdown();
        System.out.println( "Done...");
        
        
    }
}

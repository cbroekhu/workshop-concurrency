package solution.com.workshop.concurrency.callback;

public interface Callback {

    void whenDone( String message);

}
